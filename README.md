This project was a proof of concept and I've now abandoned in favour different workflows, but leaving this project here in case it can serve as inspiration to others.

----

# Elektrobox

Tool for managing development environments in containers.

## Background

This is inspired by [Toolbox], but aimed at developers who want their
development environment to be sandboxed from their desktop home directory.

The idea for this came from a script I made, called [toolbox-project], which is
a wrapper for [Toolbox], that creates a container and a `.desktop` launcher
file based on a couple of configuration files in a project directory. The
`.desktop` files launch a Terminal with an instance of [tmux] running in the
container.

## Goals

* Allow running terminal and GUI applications (typically text editors or IDEs)
  within a sandboxed development environment.
* Allow integration with the host OS
* Allow using any container, with limited functionality, if some components
  needed for integration with the host environment is missing.

## Non-goals

* Managing containers to be used in software deployments. You may be able to
  base your development containers on the same base as those used for
  deployment, but they will need to have particular properties, including of
  course installing all your development tools in it.
* Support for container engines other than Podman.

## Roadmap

- [ ] Build images based on provided Dockerfiles.
- [ ] Launch GUI applications in containers associated with a directory and
      image.
- [ ] Launch terminal applications (in a terminal emulator in the host
      environment) in containers associated with a directory and image.
- [ ] Update user configuration in the container from a "dotfiles" repository.
- [ ] Show list of known development containers, similar to the project
      selector screen of some IDEs.
- [ ] Generate `.desktop` launcher files for a particular application in a
      container.
- [ ] Allow copy/paste between the applications running in the container and
      the desktop.
- [ ] Delete containers, images and `.desktop` files related to a project.
- [ ] CLI application to perform some common actions.

[Toolbox]: https://github.com/containers/toolbox
[toolbox-project]: https://gitlab.com/bkhl/toolboxes/-/blob/master/scripts/toolbox-project
[tmux]: https://github.com/tmux/tmux
