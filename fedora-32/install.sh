#!/bin/bash

set -xueo pipefail

# Include documentation with RPM installations.
sed -i '/tsflags=nodocs/d' /etc/dnf/dnf.conf

# Needed for the xargs construct below (and we want it anyway).
dnf -y install findutils

# Reinstall some packages with documentation included.
xargs dnf -y reinstall <<HERE
acl
bash
chkconfig
curl
dbus-daemon
gawk
grep
gzip
libcap
openssl
p11-kit
pam
python3
rpm
rpm-plugin-systemd-inhibit
sed
systemd
tar
HERE

# Install basic development tools
dnf -y groupinstall 'Development Tools' 'C Development Tools and Libraries'

# Install some additonal useful packages
xargs dnf -y install <<HERE
bash-completion
bc
bind-utils
bzip2
diffutils
dnf-plugins-core
dos2unix
file
flatpak-spawn
fpaste
git
glibc-langpack-en
gnupg
gnupg2-smime
hostname
iputils
jwhois
keyutils
krb5-libs
less
lsof
man-db
man-pages
mlocate
moreutils
mtr
openssh-clients
passwd
pigz
procps-ng
rsync
sha
shadow-utils
sudo
tcpdump
time
traceroute
unzip
vte-profile
wget
which
wl-clipboard
words
xz
zip
HERE

dnf clean all

rm -rf /var/cache/dnf
